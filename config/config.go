package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

type MySQLConfig struct {
	Dbname   string `json:"dbname"`
	User     string `json:"username"`
	Password string `json:"password"`
	Host     string
	Port     string
	SSLMode  string
}

func LoadMySQLConfig(jsonFile string) (*MySQLConfig, error) {
	if _, err := os.Stat(jsonFile); os.IsNotExist(err) {
		fmt.Println("Load mysql config error:", err)
		return nil, err
	}
	if jsonFile, err := ioutil.ReadFile(jsonFile); err != nil {
		fmt.Println("Read mysql config file error:", err)
		return nil, err
	} else {
		var mysqlConfig MySQLConfig
		if err := json.Unmarshal(jsonFile, &mysqlConfig); err != nil {
			fmt.Println("Read mysql config file error:", err)
			return nil, err
		}
		return &mysqlConfig, nil
	}
}
