package model

import "time"

type GetAccessTokenResponse struct {
	AccessToken string    `json:"accessToken"`
	ExpiresOn   time.Time `json:"expiresOn"`
	TokenType   string    `json:"tokenType"`
}

type CreatePaymentRequestResponse struct {
	PaymentRequestID   string    `json:"paymentRequestId"`
	TotalAmount        string    `json:"totalAmount"`
	CurrencyCode       string    `json:"currencyCode"`
	NotificationURI    string    `json:"notificationUri"`
	AppSuccessCallback string    `json:"appSuccessCallback"`
	AppFailCallback    string    `json:"appFailCallback"`
	CreatedTime        time.Time `json:"createdTime"`
	EffectiveDuration  int       `json:"effectiveDuration"`
	WebLink            string    `json:"webLink"`
	AppLink            string    `json:"appLink"`
	StatusDescription  string    `json:"statusDescription"`
	StatusCode         string    `json:"statusCode"`
}

type CancelPaymentRequestResponse struct {
	PaymentRequestID   string    `json:"paymentRequestId"`
	TotalAmount        string    `json:"totalAmount"`
	CurrencyCode       string    `json:"currencyCode"`
	AppSuccessCallback string    `json:"appSuccessCallback"`
	AppFailCallback    string    `json:"appFailCallback"`
	CreatedTime        time.Time `json:"createdTime"`
	EffectiveDuration  int       `json:"effectiveDuration"`
	StatusDescription  string    `json:"statusDescription"`
	StatusCode         string    `json:"statusCode"`
}

type GetPaymentRequestStatusResponse struct {
	PaymentRequestID  string `json:"paymentRequestId"`
	StatusCode        string `json:"statusCode"`
	StatusDescription string `json:"statusDescription"`
}

type GetTransactionsResponse struct {
	ListDate     time.Time            `json:"listDate"`
	Transactions []TransactionDetails `json:"transactions"`
}

type TransactionDetails struct {
	FeeAmount                    float64   `json:"feeAmount"`
	FeeCurrencyCode              string    `json:"feeCurrencyCode"`
	OrderDescription             string    `json:"orderDescription"`
	OrderID                      string    `json:"orderId"`
	PayerID                      string    `json:"payerId"`
	PayerName                    string    `json:"payerName"`
	ReasonCode                   string    `json:"reasonCode"`
	Refundable                   bool      `json:"refundable"`
	StatusCode                   string    `json:"statusCode"`
	StatusDescription            string    `json:"statusDescription"`
	TransactionAmount            float64   `json:"transactionAmount"`
	TransactionCurrencyCode      string    `json:"transactionCurrencyCode"`
	TransactionID                string    `json:"transactionId"`
	TransactionSource            string    `json:"transactionSource"`
	TransactionSourceDescription string    `json:"transactionSourceDescription"`
	TransactionTime              time.Time `json:"transactionTime"`
	TransactionType              string    `json:"transactionType"`
	TransactionTypeDescription   string    `json:"transactionTypeDescription"`
}

type RefundResponse struct {
	FeeAmount          float64 `json:"feeAmount"`
	FeeCurrencyCode    string  `json:"feeCurrencyCode"`
	PayerID            string  `json:"payerId"`
	ReasonCode         string  `json:"reasonCode"`
	ReasonMessage      string  `json:"reasonMessage"`
	RefundAmount       float64 `json:"refundAmount"`
	RefundCurrencyCode string  `json:"refundCurrencyCode"`
	RefundID           string  `json:"refundId"`
	TransactionID      string  `json:"transactionId"`
}
