#!/bin/bash

set -e

WORKING_DIR=$(dirname "${BASH_SOURCE[0]}")
BUILD_DIR=$WORKING_DIR/build
# DISTRO=linux_x64
COMPRESSION_METHOD="xz"
DISTRO=darwin
VERSION=""
BUILD_DATE=$(date "+%Y-%m-%d@%H:%M:%S@%Z")
#BUILD_TAGS="''"
BUILD_TAGS="'EVDEV'"
NUM_RE='^[0-9]+$'

#CHECK_TEST=1

if [ $# == 0 ]; then
    echo -n 'VERSION_DERIVATIVE [optional]: '
    read VERSION_DERIVATIVE
    echo -n "VERSION_MAJOR: "
    read VERSION_MAJOR
    echo -n "VERSION_MINOR: "
    read VERSION_MINOR
    echo -n 'VERSION_RELEASE: '
    read VERSION_RELEASE
    echo -n 'VERSION_BUILD [optional]: '
    read VERSION_BUILD
    echo -n 'Compression: (xz) '
    read COMPRESSION_METHOD
elif [ $# == 3 ]; then
    VERSION_MAJOR=$1
    VERSION_MINOR=$2
    VERSION_DERIVATIVE=$3
    # COMPRESSION_METHOD="xz"
else
    echo "Error: Not enough parameters, enter either 3 or none parameters"
    exit 1
fi

COMPRESSION_METHOD=${COMPRESSION_METHOD:-xz}

if ! [[ $VERSION_MAJOR =~ $NUM_RE ]] || ! [[ $VERSION_MINOR =~ $NUM_RE ]] || ! [[ $VERSION_RELEASE =~ $NUM_RE ]]; then
    echo "[error] some field is not a number" >&2
    exit 1
elif [ "$COMPRESSION_METHOD" != "xz" ] && [ "$COMPRESSION_METHOD" != "gz" ]; then
    echo "[error] Wrong COMPRESSION_METHOD" >&2
    exit 1
elif ! [ ${#VERSION_BUILD} -eq 0 ]; then
    VERSION="${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_RELEASE}.${VERSION_BUILD}"
elif [ ${#VERSION_BUILD} -eq 0 ]; then
    VERSION="${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_RELEASE}"
else
    echo "[error] Wrong VERSION_BUILD" >&2
    exit 1
fi

VERSION_DERIVATIVE=$(echo "$VERSION_DERIVATIVE" | tr '[:upper:]' '[:lower:]')
VERSION=$(echo "$VERSION" | tr '[:upper:]' '[:lower:]')
VERSION_BUILD=$(echo "$VERSION_BUILD" | tr '[:upper:]' '[:lower:]')
if [ ${#VERSION_DERIVATIVE} -ne 0 ]; then
    VERSION="${VERSION_DERIVATIVE}_${VERSION}"
fi

if [ -d $BUILD_DIR ]; then
    echo "Removing previous build..."
    rm -rf $BUILD_DIR
fi

echo "Building payme-go-server Version: $VERSION for Distribution: $DISTRO"
echo "go build --tags $BUILD_TAGS -ldflags "-X main.BUILD=$BUILD_TAGS -X main.BUILD_DATE=$BUILD_DATE -X main.VERSION_MAJOR=$VERSION_MAJOR -X main.VERSION_MINOR=$VERSION_MINOR -X main.VERSION_RELEASE=$VERSION_RELEASE -X main.VERSION_DERIVATIVE=$VERSION_DERIVATIVE" -o payme-go-server main.go"
set +e
build_result=99
mkdir -p $BUILD_DIR

if [[ "$DISTRO" == "linux_x64" ]]; then
    echo "11111111"
    env GOOS=linux GOARCH=amd64 go build --tags $BUILD_TAGS -ldflags "-X main.BUILD=$BUILD_TAGS -X main.BUILD_DATE=$BUILD_DATE -X main.VERSION_MAJOR=$VERSION_MAJOR -X main.VERSION_MINOR=$VERSION_MINOR -X main.VERSION_RELEASE=$VERSION_RELEASE -X main.VERSION_DERIVATIVE=$VERSION_DERIVATIVE" -o build/payme-go-server main.go
elif [[ "$DISTRO" == "darwin" ]]; then
    echo "2222222"
    env GOOS=darwin GOARCH=amd64 go build --tags $BUILD_TAGS -ldflags "-X main.BUILD=$BUILD_TAGS -X main.BUILD_DATE=$BUILD_DATE -X main.VERSION_MAJOR=$VERSION_MAJOR -X main.VERSION_MINOR=$VERSION_MINOR -X main.VERSION_RELEASE=$VERSION_RELEASE -X main.VERSION_DERIVATIVE=$VERSION_DERIVATIVE" -o build/payme-go-server main.go
fi
cp -r config build/
cp payme-go-server.service build/
tar -vcf payme-go-server_$VERSION.tar.xz build

build_result=$?
if [ $build_result -ne 0 ]; then
    echo "Build Failed."
    exit 3
else
    echo "Finished building payme-go-server"
fi

# if [ "$COMPRESSION_METHOD" = "gz" ] ; then
# 	tar -zcf payme-go-server_$VERSION.tar.gz $DISTRO/* $DISTRO/.payme-go-server; cd $WORKING_DIR
#     echo "Created zip file payme-go-server_$VERSION.tar.gz from $DISTRO"
# else
# 	XZ_OPT=-e9 tar -cJf payme-go-server_$VERSION.tar.xz $DISTRO/* $DISTRO/.payme-go-server; cd $WORKING_DIR
#     echo "Created zip file payme-go-server_$VERSION.tar.xz from $DISTRO"
# fi
