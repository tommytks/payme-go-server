module payme-go-server

go 1.14

require (
	github.com/99designs/httpsignatures-go v0.0.0-20170731043157-88528bf4ca7e
	github.com/appleboy/gin-jwt v2.5.0+incompatible
	github.com/gin-gonic/gin v1.6.3
	github.com/joho/godotenv v1.3.0
	github.com/satori/go.uuid v1.2.0
	github.com/spf13/cobra v1.0.0
	github.com/stretchr/testify v1.4.0
	github.com/unrolled/secure v1.0.8
	gopkg.in/dgrijalva/jwt-go.v3 v3.2.0 // indirect
)
