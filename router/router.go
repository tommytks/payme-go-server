package router

import (
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/99designs/httpsignatures-go"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	_ "github.com/joho/godotenv/autoload"
	uuid "github.com/satori/go.uuid"

	// uuid "github.com/satori/go.uuid"
	"github.com/unrolled/secure"

	. "payme-go-server/datastore/model"
)

var accessToken GetAccessTokenResponse

var baseURL string
var clientID string
var clientSecret string
var version string

func init() {
	__DEV__ := os.Getenv("__DEV__")
	if strings.ToUpper(__DEV__) == "TRUE" {
		_ = godotenv.Load("config/dev/payme_config.env")
	} else {
		_ = godotenv.Load("config/production/payme_config.env")
	}
	baseURL = os.Getenv("BASE_URL")
	clientID = os.Getenv("CLIENT_ID")
	clientSecret = os.Getenv("CLIENT_SECRET")
	version = os.Getenv("VERSION")
}
func SetupRouter() *gin.Engine {
	// Disable Console Color
	// gin.DisableConsoleColor()
	r := gin.Default()
	r.Use(TlsHandler())
	r.GET("/hello", helloHandler)
	r.POST("getAccessToken", GetAccessToken)
	authorized := r.Group("/payments")
	authorized.Use(beforeSend())
	{
		authorized.POST("/paymentrequests", CreatePaymentRequest)
		authorized.GET("/paymentrequests/:paymentRequestId", GetPaymentStatus)
		authorized.PUT("/paymentrequests/:paymentRequestId/cancel", CancelPaymentRequest)
		authorized.GET("/transactions", GetTransactions)
		authorized.GET("/transactions/:transactionId", GetTransactionsByID)
		authorized.POST("/transactions/:transactionId/refund", Refund)
		authorized.POST("/generateQRCode", GenerateQRCode)
	}

	return r
}

func GetAccessToken(c *gin.Context) {
	method := "POST"

	payload := strings.NewReader("client_id=" + clientID + "&client_secret=" + clientSecret)

	client := &http.Client{}
	req, err := http.NewRequest(method, baseURL, payload)

	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Api-Version", version)
	res, err := client.Do(req)
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	var data map[string]interface{}
	if res.StatusCode == 200 {
		json.Unmarshal(body, &accessToken)
		c.JSON(200, gin.H{"token": accessToken.AccessToken})
	} else {
		c.JSON(res.StatusCode, data)
	}
	json.Unmarshal(body, &data)
	c.JSON(res.StatusCode, data)
}

func refreshToken() {
	method := "POST"

	payload := strings.NewReader("client_id=" + clientID + "&client_secret=" + clientSecret)

	client := &http.Client{}
	req, err := http.NewRequest(method, baseURL, payload)

	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Api-Version", version)
	res, err := client.Do(req)
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if res.StatusCode == 200 {
		json.Unmarshal(body, &accessToken)
	}
}

func CreatePaymentRequest(c *gin.Context) {
	t := time.Now()
	date := strings.Split(t.String(), " ")
	dateStr := date[0] + "T" + date[1] + "Z"

	method := "POST"

	client := &http.Client{}
	req, err := http.NewRequest(method, baseURL, c.Request.Body)

	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("Api-Version", version)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Accept-Language", "en-US")
	// req.Header.Add("Trace-Id", u1)
	req.Header.Add("Request-Date-Time", dateStr)
	req = addHeader(req)
	httpSignature(req)
	b, err := ioutil.ReadAll(req.Body)
	if err != nil {
		panic(err)
	}
	req.Header.Add("Digest", computeDigest(string(b)))

	res, err := client.Do(req)
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)

	fmt.Println(string(body))
}
func GetPaymentStatus(c *gin.Context) {

	method := "GET"

	client := &http.Client{}
	req, err := http.NewRequest(method, baseURL, nil)

	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Accept-Language", "en-US")
	req = addHeader(req)

	res, err := client.Do(req)
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)

	fmt.Println(string(body))
}
func CancelPaymentRequest(c *gin.Context) {
	method := "PUT"

	payload := strings.NewReader("")

	client := &http.Client{}
	req, err := http.NewRequest(method, baseURL, payload)

	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Accept-Language", "en-US")
	req = addHeader(req)

	res, err := client.Do(req)
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)

	fmt.Println(string(body))
}
func GetTransactions(c *gin.Context) {
	// u1 := uuid.NewV4()
	method := "GET"

	client := &http.Client{}
	req, err := http.NewRequest(method, baseURL, nil)

	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Accept-Language", "en-US")
	req = addHeader(req)

	res, err := client.Do(req)
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)

	fmt.Println(string(body))
}
func GetTransactionsByID(c *gin.Context) {
	// u1 := uuid.NewV4()
	method := "GET"

	client := &http.Client{}
	req, err := http.NewRequest(method, baseURL, nil)

	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Accept-Language", "en-US")
	req = addHeader(req)

	res, err := client.Do(req)
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)

	fmt.Println(string(body))
}
func Refund(c *gin.Context) {
	// u1 := uuid.NewV4()
	method := "POST"
	body, err := ioutil.ReadAll(c.Request.Body)
	payload := strings.NewReader(string(body))

	client := &http.Client{}
	req, err := http.NewRequest(method, baseURL, payload)

	if err != nil {
		fmt.Println(err)
	}
	b, err := ioutil.ReadAll(req.Body)
	if err != nil {
		panic(err)
	}
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Accept-Language", "en-US")
	req.Header.Add("Digest", computeDigest(string(b)))
	req.Header.Add("X-HSBC-DeviceId", "{{deviceId}}")
	req = addHeader(req)

	res, err := client.Do(req)
	defer res.Body.Close()
	body, err = ioutil.ReadAll(res.Body)
	fmt.Println(string(body))
}

// TODO
func GenerateQRCode(c *gin.Context) {}

func TlsHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		secureMiddleware := secure.New(secure.Options{
			SSLRedirect: true,
			SSLHost:     "localhost:8080",
		})
		err := secureMiddleware.Process(c.Writer, c.Request)

		// If there was an error, do not continue.
		if err != nil {
			return
		}

		c.Next()
	}
}
func addHeader(req *http.Request) *http.Request {
	t := time.Now()
	date := strings.Split(t.String(), " ")
	dateStr := date[0] + "T" + date[1] + "Z"
	u1 := uuid.NewV4().String()

	req.Header.Add("Trace-Id", u1)
	req.Header.Add("Request-Date-Time", dateStr)
	req.Header.Add("Api-Version", version)
	req.Header.Add("Authorization", "Bearer "+accessToken.AccessToken)
	return req
}
func helloHandler(c *gin.Context) {
	c.JSON(200, gin.H{
		"message": "pong",
	})
}
func beforeSend() gin.HandlerFunc {
	return func(c *gin.Context) {
		layout := "2006-01-02T15:04:05.000Z"
		timeNow := time.Now()
		tExpireOn, err := time.Parse(layout, accessToken.ExpiresOn.String())
		if err != nil {
			fmt.Println(err)
		}
		fmt.Printf("expire on: %v", accessToken.ExpiresOn.String())
		if diff := tExpireOn.Sub(timeNow); diff < time.Duration(30*time.Second) {
			refreshToken()
		}
	}
}
func httpSignature(request *http.Request) {
	headerList := httpsignatures.HeaderList{httpsignatures.RequestTarget, "Api-Version", "Request-Date-Time", "Content-Type", "Digest", "Accept", "Trace-Id", "Authorization"}
	signer := httpsignatures.NewSigner(
		httpsignatures.AlgorithmHmacSha256,
		headerList...,
	)
	signer.SignRequest("KeyId", "Key", request)
}
func computeDigest(messageBody string) string {
	h := sha256.Sum256([]byte(messageBody))
	return "SHA256=" + base64.StdEncoding.EncodeToString(h[:])
}
