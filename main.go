package main

import (
	"fmt"
	"os"
	"payme-go-server/router"
	"time"

	"github.com/spf13/cobra"
)

const SOFTWARE_NAME = "payme go server"

var (
	BUILD_TAGS         = ""
	BUILD_DATE         string
	VERSION_MAJOR      = "0"
	VERSION_MINOR      = "0"
	VERSION_RELEASE    = "0"
	VERSION_DERIVATIVE = "unknown"
)

func init() {

	if len(BUILD_DATE) == 0 {
		BUILD_DATE = "Now --> " + time.Now().Local().Format(time.RFC3339)
	}
	cobra.OnInitialize()
	RootCmd.AddCommand(versionCmd)
}

var RootCmd = &cobra.Command{
	Use:   SOFTWARE_NAME,
	Short: SOFTWARE_NAME + " core",
	Long:  `payme-go-server: A Payment API Service which handles payment requests from clients with PayMe API`,
	Run: func(cmd *cobra.Command, args []string) {
		server()
	},
}

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the version number of " + SOFTWARE_NAME,
	Run: func(cmd *cobra.Command, args []string) {
		if len(VERSION_DERIVATIVE) != 0 {
			fmt.Println(SOFTWARE_NAME)
			fmt.Println("Version   :", VERSION_MAJOR+"."+VERSION_MINOR+"."+VERSION_RELEASE+"_"+VERSION_DERIVATIVE)
			fmt.Println("Build     :", BUILD_TAGS)
			fmt.Println("Build Date:", BUILD_DATE)
		} else {
			fmt.Println(SOFTWARE_NAME)
			fmt.Println("Version   :", VERSION_MAJOR+"."+VERSION_MINOR+"."+VERSION_RELEASE)
			fmt.Println("Build     :", BUILD_TAGS)
			fmt.Println("Build Date:", BUILD_DATE)
		}
	},
}

func main() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(0)
	}
}

func server() {
	versionString := ""
	if len(VERSION_DERIVATIVE) != 0 {
		versionString = VERSION_MAJOR + "." + VERSION_MINOR + "." + VERSION_RELEASE + "_" + VERSION_DERIVATIVE
	} else {
		versionString = VERSION_MAJOR + "." + VERSION_MINOR + "." + VERSION_RELEASE
	}
	fmt.Println("----------------------------------------")
	fmt.Println("****************************************")
	fmt.Println("----------------------------------------")
	fmt.Println("Starting payment api version:", versionString)
	fmt.Println("Finished payment Configuration and Setup")
	fmt.Println("----------------------------------------")
	fmt.Println("-------------------------")
	fmt.Println("Payment API Service by PayMe Start Serving Now")
	fmt.Println("-------------------------")
	r := router.SetupRouter()
	// db, err := sql.Open("mysql", "user:password@/database")
	// if err != nil {
	// 	panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	// }
	// defer db.Close()
	// Listen and Server in 0.0.0.0:443
	r.Run(":443")
	// r.Run(":8080")
	// defer config.closeDB()
}
